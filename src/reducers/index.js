import { combineReducers } from 'redux';

const songsReducer = () => {
    return [
        {title: 'Nirvana', duration: '4:15'},
        {title: 'Metallica', duration: '2:10'},
        {title: 'Blunk 182', duration: '4:15'},
        {title: 'RHCP', duration: '3:55'}
    ];
};

const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === 'SONG_SELECTED') {
      return action.payload;
  }
  return selectedSong;
};

export default combineReducers({
    songs: songsReducer,
    selectedSong: selectedSongReducer
});

